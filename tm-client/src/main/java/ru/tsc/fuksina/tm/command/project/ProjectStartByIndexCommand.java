package ru.tsc.fuksina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.dto.request.ProjectChangeStatusByIndexRequest;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-start-by-index";

    @NotNull
    public static final String DESCRIPTION = "Start project by index";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectEndpoint().changeProjectStatusByIndex(new ProjectChangeStatusByIndexRequest(getToken(), index, Status.IN_PROGRESS));
    }

}
