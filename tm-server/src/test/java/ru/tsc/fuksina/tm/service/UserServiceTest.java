package ru.tsc.fuksina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.fuksina.tm.api.service.IPropertyService;
import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.exception.field.*;
import ru.tsc.fuksina.tm.exception.system.AccessDeniedException;
import ru.tsc.fuksina.tm.model.Project;
import ru.tsc.fuksina.tm.model.Task;
import ru.tsc.fuksina.tm.model.User;
import ru.tsc.fuksina.tm.repository.ProjectRepository;
import ru.tsc.fuksina.tm.repository.TaskRepository;
import ru.tsc.fuksina.tm.repository.UserRepository;
import ru.tsc.fuksina.tm.util.HashUtil;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class UserServiceTest {

    @NotNull
    private final UserRepository userRepository = new UserRepository();

    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final TaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final UserService userService = new UserService(userRepository, taskRepository, projectRepository, propertyService);

    private static long INITIAL_SIZE;

    @Before
    public void init() {
        userService.create("user_1", "pass_1", Role.USUAL);
        userService.create("user_2", "pass_2", Role.ADMIN);
        userService.create("user_3", "pass_3", "email_3");
        INITIAL_SIZE = userService.getSize();
    }

    @Test
    public void create() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", "pass"));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("log", ""));
        Assert.assertThrows(LoginExistsException.class,
                () -> userService.create("user_1", "pass", "email")
        );
        @Nullable final User user = userService.create("user_4", "pass_4");
        Assert.assertEquals(INITIAL_SIZE + 1, userService.getSize());
        Assert.assertEquals(Role.USUAL, user.getRole());
    }

    @Test
    public void createWithEmail() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", "pass", "email"));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("log", "", "email"));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.create("log", "pass", ""));
        Assert.assertThrows(LoginExistsException.class,
                () -> userService.create("user_1", "pass", "email")
        );
        Assert.assertThrows(EmailExistsException.class,
                () -> userService.create("user_4", "pass", "email_3")
        );
        @Nullable final User user = userService.create("user_4", "pass_4", "email_4");
        Assert.assertEquals(INITIAL_SIZE + 1, userService.getSize());
        Assert.assertEquals(Role.USUAL, user.getRole());
        Assert.assertEquals("email_4", user.getEmail());
    }

    @Test
    public void createWithRole() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", "pass", Role.USUAL));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("log", "", Role.USUAL));
        Assert.assertThrows(LoginExistsException.class,
                () -> userService.create("user_1", "pass", "email")
        );
        @Nullable final User user = userService.create("user_4", "pass_4", Role.ADMIN);
        Assert.assertEquals(INITIAL_SIZE + 1, userService.getSize());
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    public void findAll() {
        @NotNull final List<User> users = userService.findAll();
        Assert.assertEquals(INITIAL_SIZE, users.size());
    }

    @Test
    public void add() {
        @Nullable User user = null;
        Assert.assertNull(userService.add(user));
        user = new User();
        userService.add(user);
        Assert.assertEquals(INITIAL_SIZE + 1, userService.getSize());
    }

    @Test
    public void remove() {
        Assert.assertNull(userService.remove(null));
        @NotNull final User user = userService.create("user_4", "pass");
        @NotNull final Project project = projectRepository.create(user.getId(), "pro");
        @NotNull final Task task = taskRepository.create(user.getId(), "task");
        userService.remove(user);
        Assert.assertEquals(INITIAL_SIZE, userService.getSize());
        Assert.assertNull(userService.findOneById(user.getId()));
        Assert.assertNull(projectRepository.findOneById(project.getId()));
        Assert.assertNull(taskRepository.findOneById(task.getId()));
    }

    @Test
    public void clear() {
        userService.clear();
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void findOneByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findOneByLogin(""));
        @NotNull String login = "login";
        userService.create(login, "pass");
        Assert.assertNotNull(userService.findOneByLogin(login));
        Assert.assertEquals(login, userService.findOneByLogin(login).getLogin());
    }

    @Test
    public void findOneByEmail() {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findOneByEmail(""));
        @NotNull String email = "e-mail";
        userService.create("login", "pass", email);
        Assert.assertNotNull(userService.findOneByEmail(email));
        Assert.assertEquals(email, userService.findOneByEmail(email).getEmail());
    }

    @Test
    public void isLoginExists() {
        Assert.assertFalse(userService.isLoginExists(""));
        Assert.assertTrue(userService.isLoginExists("user_1"));
    }

    @Test
    public void isEmailExists() {
        Assert.assertFalse(userService.isEmailExists(""));
        Assert.assertTrue(userService.isEmailExists("email_3"));
    }

    @Test
    public void removeByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(""));
        @NotNull final String login = "user_1";
        userService.removeByLogin(login);
        Assert.assertNull(userService.findOneByLogin(login));
    }

    @Test
    public void setPassword() {
        Assert.assertThrows(AccessDeniedException.class, () -> userService.setPassword("", "new_pass"));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword("123", ""));
        Assert.assertNull(userService.setPassword("123", "new_pass"));
        @Nullable final User user = userService.create("logg", "pass");
        @NotNull final String pass = "new_pass";
        @Nullable final String hash = HashUtil.salt(propertyService, pass);
        userService.setPassword(user.getId(), pass);
        Assert.assertEquals(hash, user.getPasswordHash());
    }

    @Test
    public void updateUser() {
        Assert.assertThrows(AccessDeniedException.class,
                () -> userService.updateUser("", "first", "last", "middle")
        );
        Assert.assertNull(userService.updateUser("123", "first", "last", "middle"));
        @NotNull final String id = userService.findOneByLogin("user_1").getId();
        @NotNull final User user = userService.updateUser(id, "first", "last", "middle");
        Assert.assertEquals("first", user.getFirstName());
        Assert.assertEquals("last", user.getLastName());
        Assert.assertEquals("middle", user.getMiddleName());
    }

    @Test
    public void lockUserByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(""));
        @NotNull final User user = userService.findOneByLogin("user_1");
        userService.lockUserByLogin("user_1");
        Assert.assertEquals(true, user.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(""));
        @NotNull final User user = userService.findOneByLogin("user_1");
        userService.lockUserByLogin("user_1");
        userService.unlockUserByLogin("user_1");
        Assert.assertEquals(false, user.getLocked());
    }

}
